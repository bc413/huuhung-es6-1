const colorList = ["pallet", "viridian", "pewter", "cerulean", "vermillion", "lavender","celadon","saffron","fuschia","cinnabar"];

let buttonHTML = (color) => `<button class="color-button ${color}"></button>`

let dsMau = () =>{
    let htmls = colorList.map(item => buttonHTML(item)).join('\n')
    document.querySelector("#colorContainer").innerHTML = htmls;
}
dsMau()


function clickDoiMau (){
    let home = document.querySelector("#house")
    let buttons = Array.prototype.slice.call(
        document.querySelectorAll('.color-button')
        )

    if(buttons.length ){
        buttons.forEach(item => {
            item.addEventListener("click",function(e){
                e.preventDefault();
                let color = this.classList[1]
                console.log(color)
                buttons.forEach(button => button.classList.remove('active')); this.classList.add("active")
                home.removeAttribute('class')
                home.setAttribute('class','house ' + color)
            })
        })
    }
}
clickDoiMau();


// Cach 2
// let bangMau = () =>{
//     let buttons = `
//     <button class="color-button ${colorList[0]}"></button>
//     <button class="color-button ${colorList[1]}"></button>
//     <button class="color-button ${colorList[2]}"></button>
//     <button class="color-button ${colorList[3]}"></button>
//     <button class="color-button ${colorList[4]}"></button>
//     <button class="color-button ${colorList[5]}"></button>
//     <button class="color-button ${colorList[6]}"></button>
//     <button class="color-button ${colorList[7]}"></button>
//     <button class="color-button ${colorList[8]}"></button>
//     <button class="color-button ${colorList[9]}"></button>
//     <button class="color-button ${colorList[10]}"></button>
//     `
//     document.querySelector("#colorContainer").innerHTML = buttons
// }
// bangMau()

// let clickDoiMau = (idClick,idColor) =>{
//     let click = document.querySelector(idClick);
//     let home = document.querySelector("#house")
//     click.addEventListener("click",() =>{
//         console.log(idColor)
//         home.removeAttribute("class")
//         home.setAttribute("class", "house " + idColor);
//     })
// }

// clickDoiMau(".pallet","idColor")
// clickDoiMau(".viridian","viridian")
// clickDoiMau(".pewter","pewter")
// clickDoiMau(".cerulean","cerulean")