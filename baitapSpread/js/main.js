let hoverMe = document.querySelector(".heading").innerText;
let chars = [...hoverMe];

let span = (text) => `<span>${text}</span>`

let jumpText = () =>{
    let htmls = chars.map((item) => span(item)).join('\n')
    document.querySelector(".heading").innerHTML = htmls
};

jumpText()